# pink-ffmpeg

An attempt to create an easy-to-deploy ffmpeg server with docker

## Necessary enviroment variables

| variable | example | description |
| ------ | ------ | ------ |
| IOCPREFIX | BL:FFS1: | Prefix for the IOC variables that will be created |
| IOCSERVERPORT | 8081 | Server port to access service. (http://hostname:port)|
| IOCSOURCE | BL:Detector:image1 | AreaDetector record prefix that holds image data |
| IOCXSIZE | 2000 | The maximum image width; used for row profiles in the NDPluginStats plugin |
| IOCYSIZE | 2000 | The maximum image height; used for column profiles in the NDPluginStats plugin |
| IOCNELM | 1300000 | Maximum number of elements for the "(IOCSOURCE):ArrayData" record |
| IOCFTVL | USHORT | FTVL data type for the "(IOCSOURCE):ArrayData" record. Options: "STRING,CHAR,UCHAR,SHORT,USHORT,LONG,ULONG,INT64,UINT64,FLOAT,DOUBLE,ENUM" |
| IOCTYPE | Int16 | Asyn port data type for the "(IOCSOURCE):ArrayData" record. Options: "Int8, Int16, Int32, Int64, Float32 or Float64" |
| IOCMAXBYTES | 5000000 | Sets "EPICS_CA_MAX_ARRAY_BYTES" for this IOC |


