#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

## Tests
#epicsEnvSet("IOCPREFIX", "PINK:FFS:")
#epicsEnvSet("IOCSERVERPORT", "8081")
#epicsEnvSet("IOCSOURCE", "PINK:PG01:image2")
#epicsEnvSet("IOCXSIZE", "2000")
#epicsEnvSet("IOCYSIZE", "2000")
#epicsEnvSet("IOCNELM", "1300000")
#epicsEnvSet("IOCFTVL", "USHORT")
#epicsEnvSet("IOCTYPE", "Int16")
#epicsEnvSet("IOCMAXBYTES", "5000000")

## Macros
epicsEnvSet("PREFIX", "$(IOCPREFIX)")
epicsEnvSet("SRC", "$(IOCSOURCE)")
epicsEnvSet("NELM", "$(IOCNELM)")

# The port name for the detector
epicsEnvSet("PORT",   "SRC")
# The queue size for all plugins
epicsEnvSet("QSIZE",  "5")
# The maximim image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "$(IOCXSIZE)")
# The maximim image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "$(IOCYSIZE)")
# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "2048")
# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "5")
# The search path for database files
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db")
# The number of elements in the driver waveform record
epicsEnvSet("NELEMENTS", $(NELM))
# The datatype of the waveform record
epicsEnvSet("FTVL", "$(IOCFTVL)")
# The asyn interface waveform record
epicsEnvSet("TYPE", "$(IOCTYPE)")

# The default array size for epics ca clients
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "$(IOCMAXBYTES)")

# Create an NDDriverStdArrays drivers
NDDriverStdArraysConfig("$(PORT)", $(QSIZE), 0, 0)

## Load record instances
dbLoadRecords("$(NDDRIVERSTDARRAYS)/db/NDDriverStdArrays.template","P=$(PREFIX),R=ND:,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(NELEMENTS),TYPE=$(TYPE),FTVL=$(FTVL)")

# Create a standard arrays plugin, set it to get data from the NDDriverStdArrays driver.
NDStdArraysConfigure("Image1", 3, 0, "$(PORT)", 0)
NDStdArraysConfigure("Image2", 3, 0, "$(PORT)", 0)

# Waveforms
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Image1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS)")
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image2:,PORT=Image2,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=$(TYPE),FTVL=$(FTVL),NELEMENTS=$(NELEMENTS)")

# load ffmpegServer plugin
ffmpegServerConfigure($(IOCSERVERPORT))

# Load all other plugins using commonPlugins.cmd
< ${TOP}/iocBoot/${IOC}/commonPlugins.cmd

## Load record instances
dbLoadRecords("${TOP}/iocBoot/${IOC}/ffsup.db","P=$(PREFIX)ND:,SRC=$(SRC), NELM=$(NELM)")

cd "${TOP}/iocBoot/${IOC}"

# autosave settings
set_requestfile_path("$(ADCORE)/ADApp/Db")
set_requestfile_path("$(NDDRIVERSTDARRAYS)/NDDriverStdArraysApp/Db")
set_requestfile_path("$(CALC)/calcApp/Db")
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")

## Start any sequence programs
#seq sncxxx,"user=epics"
